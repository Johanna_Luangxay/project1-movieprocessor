package movies.tests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import movies.importer.ImdbImporter;
/**
 * A JUnit Test Case that tests the functionality of
 * ImdbImporter class.
 * 
 * @author	Vashti Lanz Rubio
 * @version	10/26/20
 * */
class ImdbImporterTest {

	/**
	 * A test case that checks if the process() method ignores the Strings in ArrayList input
	 * that has over or under the required length when used split() method and turned into a String array.
	 */
	@Test
	void testInvalidLength() {
		ArrayList<String> list = new ArrayList<String>();
		String valid = "1\t2\t3\t4\t5\t6\t7\t8\t9\t10\t11\t12\t13\t14\t15\t16\t17\t18\t19\t20\t21\t22";
		String invalid_over = "1\t2\t3\t4\t5\t6\t7\t8\t9\t10\t11\t12\t13\t14\t15\t16\t17\t18\t19\t20\t21\t22\t23";
		String invalid_under = "1\t2\t3\t4\t5\t6\t7\t8\t9\t10\t11\t12\t13\t14\t15\t16\t17\t18\t19\t20\t21";
		list.add(valid);
		list.add(invalid_over);
		list.add(invalid_under);
		
		ImdbImporter imdb_processor = new ImdbImporter("","");
		ArrayList<String> result = imdb_processor.process(list);
		
		ArrayList<String> expected = new ArrayList<String>();
		expected.add("2\t4\t7\timdb");
		
		assertTrue(expected.equals(result));
	}
	/**
	 * A test case that checks if the process() method functions when
	 * Strings in the ArrayList input have empty values.
	 */
	@Test
	void testEmptyStrings() {
		ArrayList<String> list = new ArrayList<String>();
		String sample1 = "1\t\t3\t\t5\t6\t\t8\t9\t10\t11\t12\t13\t14\t15\t16\t17\t18\t19\t20\t21\t22";
		String sample2 = "1\t\t3\t4\t5\t6\t7\t8\t9\t10\t11\t12\t13\t14\t15\t16\t17\t18\t19\t20\t21\t";
		String sample3 = "\t2\t3\t\t5\t6\t\t8\t9\t10\t11\t12\t13\t14\t15\t16\t17\t18\t19\t20\t21\t22";
		list.add(sample1);
		list.add(sample2);
		list.add(sample3);
		
		ImdbImporter imdb_processor = new ImdbImporter("","");
		ArrayList<String> result = imdb_processor.process(list);
		
		ArrayList<String> expected = new ArrayList<String>();
		expected.add("\t\t\timdb");
		expected.add("\t4\t7\timdb");
		expected.add("2\t\t\timdb");
		
		assertTrue(expected.equals(result));
	}
}
