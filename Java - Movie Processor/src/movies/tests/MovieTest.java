package movies.tests;
import movies.importer.Movie;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
/**
 * A JUnit Test Case that tests the functionality of 
 * Movie class.
 * 
 * @author	Vashti Lanz Rubio
 * @version	11/02/20
 */
class MovieTest {
	/**
	 * A test case that checks if the toString() method of the Movie class
	 * returns the expected format.
	 */
	@Test
	void testToString() {
		Movie m = new Movie("Title","2020","90","imdb");
		String result = m.toString();
		String expected = "Title\t2020\t90\timdb";
		assertEquals(expected,result);		
	}
	/**
	 * A test case that checks if the Movie class properly stores the release year
	 * and its getReleaseYear() method returns the expected value.
	 */
	@Test
	void testReleaseYear() {
		Movie m = new Movie("Title","2020","90","imdb");
		String result = m.getReleaseYear();
		String expected = "2020";
		assertEquals(expected,result);		
	}
	/**
	 * A test case that checks if the Movie class properly stores the runtime
	 * and its getRunTime() method returns the expected value.
	 */
	@Test
	void testRuntime() {
		Movie m = new Movie("Title","2020","90","imdb");
		String result = m.getRuntime();
		String expected = "90";
		assertEquals(expected,result);	
	}
	/**
	 * A test case that checks if the Movie class properly stores the title
	 * and its getTitle() method returns the expected value.
	 */
	@Test
	void testTitle() {
		Movie m = new Movie("Title","2020","90","imdb");
		String result = m.getTitle();
		String expected = "Title";
		assertEquals(expected,result);	
	}
	/**
	 * A test case that checks if the Movie class properly stores the source
	 * and its getSource() method returns the expected value.
	 */
	@Test
	void testSource() {
		Movie m = new Movie("Title","2020","90","imdb");
		String result = m.getSource();
		String expected = "imdb";
		assertEquals(expected,result);	
	}
	/**
	 * A test case that checks if equals() method returns the correct boolean
	 * in terms of checking the titles.
	 */
	@Test
	void testEqualsTitle() {
		Movie m = new Movie("Title","2020","90","imdb");
		Movie invalid_title1 = new Movie("Title2","2020","90","imdb");
		Movie invalid_title2 = new Movie("Titl","2020","90","imdb");
		Movie valid_title = new Movie("Title","2020","90","kaggle");
		
		assertTrue(m.equals(valid_title));
		assertFalse(m.equals(invalid_title1));
		assertFalse(m.equals(invalid_title2));
	}
	/**
	 * A test case that checks if equals() method returns the correct boolean
	 * in terms of checking the releaseYears.
	 */
	@Test
	void testEqualsReleaseYear() {
		Movie m = new Movie("Title","2020","90","imdb");
		Movie invalid_releaseYear1 = new Movie("Title","2021","90","imdb");
		Movie invalid_releaseYear2 = new Movie("Title","2019","90","imdb");
		Movie valid_releaseYear = new Movie("Title","2020","90","kaggle");
		
		assertTrue(m.equals(valid_releaseYear));
		assertFalse(m.equals(invalid_releaseYear1));
		assertFalse(m.equals(invalid_releaseYear2));
	}
	/**
	 * A test case that checks if equals() method returns the correct boolean
	 * in terms of checking the VALID runtimes.
	 */
	@Test
	void testEqualsValidRuntime() {
		Movie m = new Movie("Title","2020","90","imdb");
		Movie valid_runtime1 = new Movie("Title","2020","90","imdb");
		Movie valid_runtime2 = new Movie("Title","2020","95","kaggle");
		Movie valid_runtime3 = new Movie("Title","2020","85","imdb");
		
		assertTrue(m.equals(valid_runtime1));
		assertTrue(m.equals(valid_runtime2));
		assertTrue(m.equals(valid_runtime3));
	}
	/**
	 * A test case that checks if equals() method returns the correct boolean
	 * in terms of checking the INVALID runtimes.
	 */
	@Test
	void testEqualsInvalidRuntime() {
		Movie m = new Movie("Title","2020","90","imdb");
		Movie invalid_runtime1 = new Movie("Title","2020","96","imdb");
		Movie invalid_runtime2 = new Movie("Title","2020","84","kaggle");
		
		assertFalse(m.equals(invalid_runtime1));
		assertFalse(m.equals(invalid_runtime2));
	}

}
