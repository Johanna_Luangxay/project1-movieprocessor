package movies.tests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import movies.importer.Normalizer;
/**
 * A JUnit Test Case that tests the functionality of
 * Normalizer class.
 * 
 * @author	Johanna Luangxay
 * @version	2020/10/29
 * */
class NormalizerTest {
	
	/**
	 * A test case that checks if the process() method ignores the rows from the input that has under or over
	 * the max number of field
	 * @return void
	 */
	@Test
	void testInvalidLength() 
	{
		Normalizer n = new Normalizer("","");
		ArrayList<String> testList = new ArrayList<String>();
		
		String validRow = "1\t2\t3\t4";
		String invalidUnderRow = "I1\tI2\tI3";
		String invalidUpperRow = "I1\tI2\tI3\tI4\tI5";
		
		testList.add(validRow);
		testList.add(invalidUnderRow);
		testList.add(invalidUpperRow);
		
		ArrayList<String> resultList = n.process(testList);
		
		ArrayList<String> expectedList = new ArrayList<String>();
		expectedList.add("1\t2\t3\t4");
		
		assertTrue(expectedList.equals(resultList));	
	}
	
	/**
	 * A test case that checks if the process() method convert the title field into lower case 
	 * without changing anything to the symbols and numbers
	 * @return void
	 */
	@Test
	void testLowerCaseTitle()
	{
		Normalizer n = new Normalizer("","");
		ArrayList<String> testList = new ArrayList<String>();
		
		String row1 = "TITLE\tYEAR\tRUNTIME\tSOURCE";
		String row2 = "TiTlE 123456\tYear\tRuntime\tSource";
		String row3 = "title ?! Where Is It5\tyear\truntime\tsource";
		
		testList.add(row1);
		testList.add(row2);
		testList.add(row3);
		
		ArrayList<String> resultList = n.process(testList);
		
		ArrayList<String> expectedList = new ArrayList<String>();
		
		String expectedRow1 = "title\tYEAR\tRUNTIME\tSOURCE";
		String expectedRow2 = "title 123456\tYear\tRuntime\tSource";
		String expectedRow3 = "title ?! where is it5\tyear\truntime\tsource";
		
		expectedList.add(expectedRow1);
		expectedList.add(expectedRow2);
		expectedList.add(expectedRow3);
		
		assertTrue(expectedList.equals(resultList));	
	}
	
	/**
	 * A test case that checks if the process() method keep only the 1st "word" in the runtime field.
	 * @return void
	 */
	@Test
	void testKeepFirstWord()
	{
		Normalizer n = new Normalizer("","");
		ArrayList<String> testList = new ArrayList<String>();
		
		String row1 = "this is the title\tThis is the Year\t210 minutes\tThis is the source";
		String row2 = "title\tYear\t90\tSource";
		String row3 = "this title\tThis Year\t120 minutes is the runtime\tThis source";
		
		testList.add(row1);
		testList.add(row2);
		testList.add(row3);
		
		ArrayList<String> resultList = n.process(testList);
		
		ArrayList<String> expectedList = new ArrayList<String>();
		
		String expectedRow1 = "this is the title\tThis is the Year\t210\tThis is the source";
		String expectedRow2 = "title\tYear\t90\tSource";
		String expectedRow3 = "this title\tThis Year\t120\tThis source";
		
		expectedList.add(expectedRow1);
		expectedList.add(expectedRow2);
		expectedList.add(expectedRow3);
		
		assertTrue(expectedList.equals(resultList));
	}
}
