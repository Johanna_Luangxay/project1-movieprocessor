package movies.tests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import movies.importer.KaggleImporter;
/**
 * A JUnit Test Case that tests the functionality of
 * KaggleImporter class.
 * 
 * @author	Johanna Luangxay
 * @version	2020/10/29
 * */
class KaggleImporterTest {
	
	/**
	 * A test case that checks if the process() method ignores the rows from the input that has under or over
	 * the max number of field
	 * @return void
	 */
	@Test
	void testInvalidLength() {
		
		KaggleImporter ki = new KaggleImporter("","");
		ArrayList<String> testList = new ArrayList<String>();
		
		String validRow = "V1\tV2\tV3\tV4\tV5\tV6\tV7\tV8\tV9\tV10\tV11\tV12\tV13\tV14\tV15\tV16\tV17\tV18\tV19\tV20\tV21";
		String invalidUnderRow = "I1\tI2\tI3\tI4\tI5\tI6\tI7\tI8\tI9\tI10\tI11\tI12\tI13\tI14\tI15\tI16\tI17\tI18\tI19\tI20";
		String invalidUpperRow = "I1\tI2\tI3\tI4\tI5\tI6\tI7\tI8\tI9\tI10\tI11\tI12\tI13\tI14\tI15\tI16\tI17\tI18\tI19\tI20\tI21\tI22";
		
		testList.add(validRow);
		testList.add(invalidUnderRow);
		testList.add(invalidUpperRow);
		
		ArrayList<String> resultList = ki.process(testList);
		
		ArrayList<String> expectedList = new ArrayList<String>();
		expectedList.add("V16\tV21\tV14\tKaggle");
		
		assertTrue(expectedList.equals(resultList));	
	}
	/**
	 * A test case that checks if the process() method do effectively take 
	 * the title, year of release, runtime from the sample file KaggleSmallTest.java
	 * @return void
	 */
	@Test
	void testRetrieveRightFields()
	{
		KaggleImporter ki = new KaggleImporter("","");
		//1st line from the KaggleSmallTest.txt file
		String fields = "Cast 1	Cast 2	Cast 3	Cast 4	Cast 5	Cast 6	Description	Director 1	Director 2	Director 3	Genre	Rating	Release Date	Runtime	Studio	Title	Writer 1	Writer 2	Writer 3	Writer 4	Year";
		ArrayList<String> testList = new ArrayList<String>();
		testList.add(fields);
		ArrayList<String> resultList = ki.process(testList);
		
		ArrayList<String> expectedList = new ArrayList<String>();
		expectedList.add("Title\tYear\tRuntime\tKaggle");
		
		assertTrue(expectedList.equals(resultList));	
	}

}
