package movies.tests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import movies.importer.Validator;
/**
 * A JUnit Test Case that tests the functionality of
 * Validator class.
 * 
 * @author	Vashti Lanz Rubio
 * @version	10/28/20
 * */
class ValidatorTest {
	/**
	 * A test case that checks if the process() method ignores the Strings in ArrayList input
	 * that has over or under the required array length when
	 * split() method is used to the Strings and turned into a String array.
	 */
	@Test
	void testInvalidLength() {
		ArrayList<String> list = new ArrayList<String>();
		String valid = "1\t2\t3\t4";
		String invalid_over = "1\t2\t3\t4\t5";
		String invalid_under = "1\t2\t3";
		list.add(invalid_over);
		list.add(valid);
		list.add(invalid_under);
		
		Validator validator_processor = new Validator("","");
		ArrayList<String> result = validator_processor.process(list);
		
		ArrayList<String> expected = new ArrayList<String>();
		expected.add(valid);
		
		assertTrue(expected.equals(result));
	}
	/**
	 * A test case that checks if the process() method ignores the Strings in ArrayList input
	 * that cannot parse the String runtime and String releaseYear into int.
	 */
	@Test
	void testNumberFormatException(){
		ArrayList<String> list = new ArrayList<String>();
		String valid = "1\t2\t3\t4";
		String invalid_year = "1\t2a\t3\t4";
		String invalid_runtime = "1\t2\t3b\t4";
		String invalid_both = "1\t2a\t3b\t4";
		list.add(valid);
		list.add(invalid_year);
		list.add(invalid_runtime);
		list.add(invalid_both);
		
		Validator validator_processor = new Validator("","");
		ArrayList<String> result = validator_processor.process(list);
		
		ArrayList<String> expected = new ArrayList<String>();
		expected.add(valid);
		
		assertTrue(expected.equals(result));
	}
	/**
	 * A test case that checks if the process() method ignores the Strings in ArrayList input
	 * that has empty String value in title, runtime, and releaseYear.
	 */
	@Test
	void testEmptyString(){
		ArrayList<String> list = new ArrayList<String>();
		String valid = "1\t2\t3\t4";
		String invalid_title = "\t2\t3\t4";
		String invalid_year = "1\t\t3\t4";
		String invalid_runtime = "1\t2\t\t4";
		String invalid_all = "\t\t\t4";
		list.add(invalid_title);
		list.add(valid);
		list.add(invalid_year);
		list.add(invalid_runtime);
		list.add(invalid_all);
		
		Validator validator_processor = new Validator("","");
		ArrayList<String> result = validator_processor.process(list);
		
		ArrayList<String> expected = new ArrayList<String>();
		expected.add(valid);
		
		assertTrue(expected.equals(result));
	}
}
