package movies.tests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import movies.importer.Deduper;
import movies.importer.Movie;
/**
 * A JUnit Test Case that tests the functionality of
 * Deduper class.
 * 
 * @author	Vashti Lanz Rubio
 * @version	11/05/2020
 * */
class DeduperTest {
	/**
	 * A test case that checks if the process() method works
	 * with varying title values.
	 */
	@Test
	void testProcessTitle() {
		Deduper d = new Deduper("","");
		ArrayList<String> list = new ArrayList<String>();
		
		Movie original = new Movie("Title","2020","100","Imdb");
		Movie m1 = new Movie("Title","2020","100","Imdb");
		Movie m2 = new Movie("Titl","2020","100","Imdb");
		Movie m3 = new Movie("Ttle","2020","100","Imdb");
		Movie m4 = new Movie("Title","2020","100","Imdb");
		Movie m5 = new Movie("Different","2020","100","Imdb");
		
		list.add(original.toString());
		list.add(m1.toString());
		list.add(m2.toString());
		list.add(m3.toString());
		list.add(m4.toString());
		list.add(m5.toString());
		
		ArrayList<String> result = d.process(list);
		ArrayList<String> expected = new ArrayList<String>();
		expected.add(original.toString());
		expected.add(m2.toString());
		expected.add(m3.toString());
		expected.add(m5.toString());
		
		assertTrue(expected.equals(result));
		
	}
	/**
	 * A test case that checks if the process() method works
	 * with varying releaseYear values.
	 */
	@Test
	void testProcessReleaseYear() {
		Deduper d = new Deduper("","");
		ArrayList<String> list = new ArrayList<String>();
		
		Movie original = new Movie("Title","2020","100","Imdb");
		Movie m1 = new Movie("Title","2020","100","Imdb");
		Movie m2 = new Movie("Title","2021","100","Imdb");
		Movie m3 = new Movie("Title","20200","100","Imdb");
		Movie m4 = new Movie("Title","2020","100","Imdb");
		Movie m5 = new Movie("Title","02020","100","Imdb");
		
		list.add(original.toString());
		list.add(m1.toString());
		list.add(m2.toString());
		list.add(m3.toString());
		list.add(m4.toString());
		list.add(m5.toString());
		
		ArrayList<String> result = d.process(list);
		ArrayList<String> expected = new ArrayList<String>();
		expected.add(original.toString());
		expected.add(m2.toString());
		expected.add(m3.toString());
		expected.add(m5.toString());
		
		assertTrue(expected.equals(result));
	}
	/**
	 * A test case that checks if the process() method works
	 * with varying runtime values.
	 */
	@Test
	void testProcessRuntime() {
		Deduper d = new Deduper("","");
		ArrayList<String> list = new ArrayList<String>();
		
		Movie original = new Movie("Title","2020","100","Imdb");
		Movie m1 = new Movie("Title","2020","100","Imdb");
		Movie m2 = new Movie("Title","2020","105","Imdb");
		Movie m3 = new Movie("Title","2020","95","Imdb");
		Movie m4 = new Movie("Title","2020","106","Imdb");
		Movie m5 = new Movie("Title","2020","94","Imdb");
		
		list.add(original.toString());
		list.add(m1.toString());
		list.add(m2.toString());
		list.add(m3.toString());
		list.add(m4.toString());
		list.add(m5.toString());
		
		ArrayList<String> result = d.process(list);
		ArrayList<String> expected = new ArrayList<String>();
		expected.add(original.toString());
		expected.add(m4.toString());
		expected.add(m5.toString());
		
		assertTrue(expected.equals(result));
		
	}
	/**
	 * A test case that checks if the process() method works
	 * with varying source values.
	 */
	@Test
	void testProcessSource() {
		Deduper d = new Deduper("","");
		ArrayList<String> list = new ArrayList<String>();
		
		Movie original = new Movie("Title","2020","100","Imdb");
		Movie m1 = new Movie("Title","2020","100","Kaggle");
		Movie m2 = new Movie("Title","2020","100","Imdb");
		Movie m3 = new Movie("Title","2020","100","Kaggle");
		
		list.add(original.toString());
		list.add(m1.toString());
		list.add(m2.toString());
		list.add(m3.toString());
		
		ArrayList<String> result = d.process(list);
		ArrayList<String> expected = new ArrayList<String>();
		expected.add("Title\t2020\t100\tKaggle;Imdb");
		

		assertTrue(expected.equals(result));
	}
}
