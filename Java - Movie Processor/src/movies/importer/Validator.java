package movies.importer;

import java.util.ArrayList;
/**
 * A concrete class the extends to the Processor abstract class
 * that is used to validate information from a processed text file.
 * 
 * @author	Vashti Lanz Rubio
 * @version	10/28/20
 */
public class Validator extends Processor{
	/**
	 * The Validator constructor.
	 * The constructor stores the source location of the processed text file 
	 * and destination for the processed version of the text file.
	 * The first line in the text file is read.
	 * 
	 * @param source	Absolute path of the processed text file.
	 * @param destination	Absolute path of storage for the validated version of the processed text file.
	 */
	public Validator(String source, String destination) {
		super(source,destination,false);
	}
	/**
	 * A method that validates each movie's title, year, and runtime information
	 * and returns a String ArrayList that only contains valid movies.
	 * 
	 * @param input	A String ArrayList containing movies with their title, year, runtime, and source.
	 * @return	A String ArrayList of valid movies.
	 */
	public ArrayList<String> process(ArrayList<String> input){
		ArrayList<String> input_processed = new ArrayList<String>();
		//The REQUIRED_LENGTH value is equal to the number of columns in the processed text file.
		final int REQUIRED_LENGTH = 4;
		
		for(String s : input) {
			String[] movieDetails = s.split("\\t");
			try {
				if(movieDetails.length != REQUIRED_LENGTH) {
					throw new ArrayIndexOutOfBoundsException();
				}
				Movie m = new Movie(movieDetails[0],movieDetails[1],movieDetails[2],movieDetails[3]);
				
				//It should be possible to parse releaseYear and runTime into int.
				//It also throws a NumberFormatException if they are empty or null.
				Integer.parseInt(m.getReleaseYear());
				Integer.parseInt(m.getRuntime());
				
				//The title is expected to be NOT null and NOT empty
				if(m.getTitle() != null && !m.getTitle().equals("")) {
					input_processed.add(m.toString());
				}
		
			} catch (ArrayIndexOutOfBoundsException e) {
				//Do nothing (Skip invalid movie)
			} catch (NumberFormatException e) {
				//Do nothing (Skip invalid movie)
			}
		}
		return input_processed;
	}
}
