package movies.importer;

/**
 * The Movie Class is an object that defines a Movie. 
 * A movie includes it's title, release year, runtime and source.
 * 
 * @author Johanna Luangxay
 * @author	Vashti Lanz Rubio
 * @version 2020-11-02
 */
public class Movie 
{
	private String title;
	private String releaseYear;
	private String runtime;
	private String source;
	
	/**
	   *This method is a constructor that assigns each given feature to this class' variables.
	   * @param title string Represent the title of the movie that was given by another class
	   * @param releaseYear string Represents the year in which the movie was release
	   * @param runtime string Represents the runtime of the movie
	   * @param source string Tells where the movie came from
	   * @return Nothing
	   * 
	   * @author Johanna Luangxay
	   */
	public Movie(String title, String releaseYear, String runtime, String source) 
	{
		this.title = title;
		this.releaseYear = releaseYear;
		this.runtime = runtime;
		this.source = source;
	}
	
	/**
	   *This method returns the release year of this movie.
	   * @return String
	   * 
	   * @author Johanna Luangxay
	   */
	public String getReleaseYear()
	{
		return releaseYear;
	}
	
	/**
	   *This method returns the runtime of this movie.
	   * @return String
	   * 
	   * @author Johanna Luangxay
	   */
	public String getRuntime()
	{
		return runtime;
	}
	
	/**
	   *This method returns the details and information of a movie, 
	   *which means its title, runtime, release and source.
	   * @return String
	   * 
	   * @author Johanna Luangxay
	   */
	public String toString()
	{
		String description = 
				title+"\t"+releaseYear+"\t"+runtime+"\t"+source;
		return description;
	}
	/**
	 * This method returns the title of this movie.
	 * 
	 * @return	String value of the title
	 * @author	Vashti Lanz Rubio
	 * */
	public String getTitle() {
		return title;
	}
	/**
	 * This method returns the source of this movie.
	 * 
	 * @return	String value of the source
	 * @author	Vashti Lanz Rubio
	 * */
	public String getSource() {
		return source;
	}
	/**
	 * This method returns the true or false based on if this Movie and
	 * Movie given are the same.
	 * The two Movies are the same if: 
	 * both Movies have the same title,
	 * both Movies have the same release year,
	 * and the difference of their runtime is no more than 5 minutes apart.
	 * 
	 * @return	String value of the source
	 * @author	Vashti Lanz Rubio
	 * */
	@Override
	public boolean equals(Object o) {
		if(!(o instanceof Movie)) {
			return false;
		}
		Movie m = (Movie) o;
		boolean hasSameTitle = title.equals(m.getTitle());
		boolean hasSameReleaseYear = releaseYear.equals(m.getReleaseYear());
		int runtimesDifference = Integer.parseInt(runtime) - Integer.parseInt(m.getRuntime());
		
		boolean hasSimilarRuntime = false;
		if(runtimesDifference <= 0 && runtimesDifference >= -5) {
			hasSimilarRuntime = true;
		}
		if(runtimesDifference >= 0 && runtimesDifference <= 5) {
			hasSimilarRuntime = true;
			
		}
		return hasSameTitle && hasSameReleaseYear && hasSimilarRuntime;
	}
}
