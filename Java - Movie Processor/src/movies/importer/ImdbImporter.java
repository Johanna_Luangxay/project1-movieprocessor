package movies.importer;

import java.util.ArrayList;
/**
 * A concrete class that extends to the Processor abstract class
 * and is used to process the imdb raw text file.
 * 
 * @author	Vashti Lanz Rubio
 * @version	10/28/20
 */
public class ImdbImporter extends Processor{
	/**
	 * The ImdbImporter constructor.
	 * The constructor stores the source location of the imdb text file
	 * and destination for the processed version of the text file.
	 * The header(first line) in the imdb text file is not read.
	 * 
	 * @param source	Absolute path of the imdb raw text file.
	 * @param destination	Absolute path of storage for the imdb processed text file.
	 */
	public ImdbImporter(String source,String destination) {
		super(source,destination,true);
	}
	/**
	 * A method that returns a String ArrayList that 
	 * contains the title, year, and runtime of each movie.
	 * 
	 * @param input	A StringList containing all information of the movies.
	 * @return	A String ArrayList containing the title, year, and runtime of the movies.
	 */
	public ArrayList<String> process(ArrayList<String> input){
		ArrayList<String> input_processed = new ArrayList<String>();
		//The REQUIRED_LENGTH value is equal to the number of columns in the imdb raw text file.
		final int REQUIRED_LENGTH = 22;
		
		for(String s : input) {
			String[] movieDetails = s.split("\\t",-1);
			if(movieDetails.length == REQUIRED_LENGTH) {
				//The indices from the movieDetails array are based 
				//on the position of the columns in the header (i.e. title, genre, duration, etc.) in the raw text file. 
				//The index 1 of movieDetails is where the title is located.
				String title = movieDetails[1];
				//The index 3 of movieDetails is where the release year is located.
				String releaseYear = movieDetails[3];
				//The index 6 of movieDetails is where the duration is located (runtime of the movie).
				String runtime = movieDetails[6];
				
				Movie m = new Movie(title,releaseYear,runtime,"Imdb");
				input_processed.add(m.toString());
			}
		}
		return input_processed;
	}
}
