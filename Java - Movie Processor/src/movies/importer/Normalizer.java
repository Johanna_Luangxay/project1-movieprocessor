package movies.importer;

import java.util.ArrayList;
/**
 * A class that extends the Processor class. 
 * It takes a list of movie and standardized the format of the title and the runtime
 * 
 * @author Johanna Luangxay
 * @version 2020/10/29
 *
 */
public class Normalizer extends Processor{
	
	/**
	 * The constructor stores the source location of the processed text file 
	 * and destination for the processed version of the text file.
	 * The first line in the text file is read.
	 * 	
	 * @param sourceDir Absolute path of the processed text file.
	 * @param outputDir Absolute path of storage for the validated version of the processed text file.
	 */
	public Normalizer(String sourceDir, String outputDir) 
	{
		super(sourceDir, outputDir, false);
	}
	
	/**
	 * The process method returns an String ArrayList
	 * where the title is convert to lower case and the runtime returns the 1st word only.
	 * 
	 * @param input String ArrayList where each row corresponds to a movie in the right format
	 * @return String ArrayList Return a String ArrayList where the format of the title and the runtime is the right one.
	 */
	public ArrayList<String> process(ArrayList<String> input)
	{
		//Creates new Array
		ArrayList<String> normalizedInput = new ArrayList<String>();
		
		//Goes through each line of movie
		for (String currentLine : input)
		{
			String[] fieldDetail = currentLine.split("\\t");
			int NUMBER_OF_FIELDS = 4;
			//Double check that there is effectively only 4 fields
			if (fieldDetail.length == NUMBER_OF_FIELDS)
			{
				//Converts the title
				String title = fieldDetail[0].toLowerCase();
				String releaseYear = fieldDetail[1];
				//Takes only the 1st word of the runtime field
				String runtime = fieldDetail[2].split(" ")[0];
				String source = fieldDetail[3];
				//Creates a new movie object
				Movie currentMovie = new Movie(title,releaseYear,runtime,source);
				//Assigns it to the new String ArrayList
				normalizedInput.add(currentMovie.toString());
			}
		}
		return normalizedInput;
	}
}
