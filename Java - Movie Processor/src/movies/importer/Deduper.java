package movies.importer;

import java.util.ArrayList;

/**
 * A class that extends the Processor class and
 * is used to process files that are doubled.
 * 
 * @author Johanna Luangxay (Writer) & Vashti Lanz Rubio (Helper)
 * @version 2020/11/05
 */
public class Deduper extends Processor{
	/**
	 * The constructor stores the source location of the processed text file 
	 * and destination for the processed version of the text file.
	 * The first line in the text file is read.
	 * 	
	 * @param sourceDir Absolute path of the processed text file.
	 * @param outputDir Absolute path of storage for the validated version of the processed text file.
	 */
	public Deduper(String sourceDir, String outputDir) {
		super(sourceDir, outputDir, false);		
	}
	
	/**
	 * The process method returns an String ArrayList
	 * It make merge doubles from different source and "removes" the one from the same source.
	 * 
	 * @param input String ArrayList where each row corresponds to a movie
	 * @return String ArrayList Return a String ArrayList with no doubles and in need merge source
	 */
	public ArrayList<String> process(ArrayList<String> input)
	{
		//Creates an array of movie
		ArrayList<Movie> processedMovieList = new ArrayList<Movie>();
		
		/*For each movie in the given String ArrayList it either adds the movie to the new list, 
		 * merge source or skip the movie*/
		for(String s : input)
		{
			String title = s.split("\\t")[0];
			String releaseYear = s.split("\\t")[1];
			String runtime = s.split("\\t")[2];
			String source = s.split("\\t")[3];

			Movie currentMovie = new Movie(title,releaseYear,runtime,source);
			
			/*Asking if the current Movie already exist in the array. 
			 * (If we already added a similar/same movie to the new array)*/
			if (processedMovieList.contains(currentMovie))
			{	
				//Taking the index of the similar/same movie of the new array
				int index = processedMovieList.indexOf(currentMovie);
				//Getting the source of the similar/same movie of the new array
				String arraySource = processedMovieList.get(index).getSource();
				
				//Asking if the source from the current movie and the one already added in the array is the same
				if (!(arraySource.equals(currentMovie.getSource()))) 
				{
					//Replace/Set the already added movie by a 'new' one where the source is merged
					currentMovie = new Movie (title,releaseYear,runtime,"Kaggle;Imdb");
					processedMovieList.set(index,currentMovie);
				}		
			}
			else
			{
				//If the movie does not exist in the new array, it adds it
				processedMovieList.add(currentMovie);
			}
		}
		
		ArrayList<String> finalMovieList = new ArrayList<String>();
		
		//Converts ArrayList<Movie> to ArrayList<String>
		for(Movie m : processedMovieList) 
		{
			finalMovieList.add(m.toString());
		}
		return finalMovieList;
	}
}
