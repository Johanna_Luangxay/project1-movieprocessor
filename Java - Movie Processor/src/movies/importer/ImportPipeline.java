package movies.importer;

import java.io.IOException;

/**
 * A class that extends the Processor class and
 * is used to process files that are doubled.
 * 
 * @author Johanna Luangxay (Writer) & Vashti Lanz Rubio (Helper)
 * @version 2020/11/05
 */
public class ImportPipeline {

	/**
	   * Main method that creates Processor and put it in an array to then call processAll to execute everything
	   * @return Nothing
	   */
	public static void main(String[] args) throws IOException
	{
		
		Processor[] pArray = new Processor[5];
		
		//Input is the directory of the Kaggle file
		String input = "C:\\Users\\Johanna L\\OneDrive\\Documents\\Dawson College\\3rd Semester\\Programming III\\Project_1_MovieImporter\\Input\\KaggleFile";
		String output = "C:\\Users\\Johanna L\\OneDrive\\Documents\\Dawson College\\3rd Semester\\Programming III\\Project_1_MovieImporter\\Output";
		pArray[0] = new KaggleImporter(input,output);
		
		//Input is the directory of the IMDB file 
		input = "C:\\Users\\Johanna L\\OneDrive\\Documents\\Dawson College\\3rd Semester\\Programming III\\Project_1_MovieImporter\\Input\\IMDBFile";
		pArray[1] = new ImdbImporter(input,output);
		
		//Input is where the outputs of the previous processor are
		input = "C:\\Users\\Johanna L\\OneDrive\\Documents\\Dawson College\\3rd Semester\\Programming III\\Project_1_MovieImporter\\Output";
		pArray[2] = new Normalizer(input,output);
		pArray[3] = new Validator(input,output);
		pArray[4] = new Deduper(input,output);
		
		processAll(pArray);
		
	}
	
	/**
	   * processAll takes in all the processor and loop through it to execute each one of them
	   * @param p inputs of a Processor[]
	   * @return void
	   */
	private static void processAll(Processor[] p) throws IOException
	{
		for(Processor currentP : p)
		{
			currentP.execute();
		}
	}

}
