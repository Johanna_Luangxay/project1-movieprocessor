package movies.importer;
import java.util.ArrayList;
/**
 * A class that extends the Processor class and
 * is used to process the Kaggle raw txt file.
 * 
 * @author Johanna Luangxay
 * @version 2020/10/29
 */
public class KaggleImporter extends Processor
{	
	/**
	 * The constructor stores the source location of the processed text file 
	 * and destination for the processed version of the text file.
	 * The first line in the text file is not read.
	 * 	
	 * @param sourceDir Absolute path of the processed text file.
	 * @param outputDir Absolute path of storage for the validated version of the processed text file.
	 */
	public KaggleImporter(String sourceDir, String outputDir)
	{
		super(sourceDir, outputDir, true);
	}
	
	/**
	 * The process method returns an String ArrayList
	 * Each line in the txt file is convert to a movie object proper format.
	 * 
	 * @param input String ArrayList where each row corresponds to a line in the raw Kaggle file
	 * @return String ArrayList Return a String ArrayList where each line was convert to a movie object proper format 
	 */
	public ArrayList<String> process(ArrayList<String> input)
	{
		//New String ArrayList
		ArrayList<String> formattedInput = new ArrayList<String>();
		//Loops through each line of the file
		for(String currentLine : input)
		{
			//Divides each field and puts it in an array
			String[] fieldDetail = currentLine.split("\\t");
			int NUMBER_OF_FIELDS = 21;
			//Make sure there is the right amount of fields for each movie
			if (fieldDetail.length == NUMBER_OF_FIELDS)
			{
				//Retrieve the fields we are interested in
				String title = fieldDetail[15];
				String releaseYear = fieldDetail[20];
				String runtime = fieldDetail[13];
				//Creates a movie object
				Movie currentMovie = new Movie(title,releaseYear,runtime,"Kaggle");
				//Puts it in the new String ArrayList
				formattedInput.add(currentMovie.toString());
			}
		}
		return formattedInput;
	}

}
